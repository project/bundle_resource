<?php

/**
 * Helper function to provide support for UUID Services
 *
 * @param $resources
 * @param $bundle_type
 * @param $bundle_definition
 */
function _uuid_services_taxonomy_term_bundle_resources_alter(&$resources, $bundle_type, $bundle_label) {
  unset($resources[$bundle_type]['operations']['create']);

  // Alter 'index' method to use UUID in parameters
  $resources[$bundle_type]['operations']['index']['callback'] = '_uuid_services_taxonomy_term_bundle_index';
  $resources[$bundle_type]['operations']['index']['file'] = array('type' => 'inc', 'module' => 'uuid_services_bundle_resource', 'name' => 'resources/uuid_services_taxonomy_term_bundle_resources');

  // Alter 'retrieve' method to use UUID enabled functions and arguments.
  $resources[$bundle_type]['operations']['retrieve']['help'] = t('Retrieve %label entities based on UUID.', array('%label' => $bundle_label));
  $resources[$bundle_type]['operations']['retrieve']['file'] = array('type' => 'inc', 'module' => 'uuid_services_bundle_resource', 'name' => 'resources/uuid_services_taxonomy_term_bundle_resources');
  $resources[$bundle_type]['operations']['retrieve']['callback'] = '_uuid_services_taxonomy_term_bundle_retrieve';
  $resources[$bundle_type]['operations']['retrieve']['access callback'] = '_uuid_services_taxonomy_term_bundle_access';
  $resources[$bundle_type]['operations']['retrieve']['access arguments'] = array('view');
  $resources[$bundle_type]['operations']['retrieve']['access arguments append'] = TRUE;
  $resources[$bundle_type]['operations']['retrieve']['args'] = array(
    // This argument isn't exposed in the service, only used internally..
    array(
      'name' => 'entity_type',
      'description' => t('The entity type.'),
      'type' => 'string',
      'default value' => 'taxonomy_term',
      'optional' => TRUE,
    ),
    array(
      'name' => 'bundle_type',
      'description' => t('The bundle type.'),
      'type' => 'string',
      'default value' => $bundle_type,
      'optional' => TRUE,
    ),
    array(
      'name' => 'fields',
      'optional' => TRUE,
      'type' => 'string',
      'description' => 'The fields to get.',
      'default value' => '*',
      'source' => array('param' => 'fields'),
    ),
    array(
      'name' => 'uuid',
      'description' => t('The %label UUID.', array('%label' => $bundle_label)),
      'type' => 'text',
      'source' => array('path' => 0),
    ),
  );

  // Alter 'update' method to use UUID enabled functions and arguments.
  $resources[$bundle_type]['operations']['update']['help'] = t('Update or create %label entities based on UUID. The payload must be formatted according to the <a href="!url">OData protocol</a>.', array('%label' => $bundle_label, '!url' => 'http://www.odata.org/developers/protocols'));
  $resources[$bundle_type]['operations']['update']['callback'] = '_uuid_services_entity_update';
  $resources[$bundle_type]['operations']['update']['access callback'] = '_uuid_services_entity_access';
  $resources[$bundle_type]['operations']['update']['access arguments'] = array('update');
  $resources[$bundle_type]['operations']['update']['access arguments append'] = TRUE;
  $resources[$bundle_type]['operations']['update']['args'] = array(
    // This argument isn't exposed in the service, only used internally..
    array(
      'name' => 'entity_type',
      'description' => t('The entity type.'),
      'type' => 'string',
      'default value' => 'taxonomy_term',
      'optional' => TRUE,
    ),
    array(
      'name' => 'uuid',
      'description' => t('The %label UUID.', array('%label' => $bundle_label)),
      'type' => 'text',
      'source' => array('path' => 0),
    ),
    array(
      'name' => 'entity',
      'description' => t('The %label entity object.', array('%label' => $bundle_label)),
      'type' => 'struct',
      'source' => 'data',
    ),
  );

  // Alter 'delete' method to use UUID enabled functions and arguments.
  $resources[$bundle_type]['operations']['delete']['help'] = t('Delete %label entities based on UUID.', array('%label' => $bundle_label));
  $resources[$bundle_type]['operations']['delete']['callback'] = '_uuid_services_entity_delete';
  $resources[$bundle_type]['operations']['delete']['access callback'] = '_uuid_services_entity_access';
  $resources[$bundle_type]['operations']['delete']['access arguments'] = array('delete');
  $resources[$bundle_type]['operations']['delete']['access arguments append'] = TRUE;
  $resources[$bundle_type]['operations']['delete']['args'] = array(
    // This argument isn't exposed in the service, only used internally..
    array(
      'name' => 'entity_type',
      'description' => t('The entity type.'),
      'type' => 'string',
      'default value' => 'taxonomy_term',
      'optional' => TRUE,
    ),
    array(
      'name' => 'uuid',
      'description' => t('The %label UUID.', array('%label' => $bundle_label)),
      'type' => 'text',
      'source' => array('path' => 0),
    ),
  );
}

/**
 * Access callback.
 *
 * @param $op
 *   The operation we are trying to do on the entity. Can only be:
 *   - "view"
 *   - "update"
 *   - "delete"
 *   See 'uuid_services_services_resources_alter()' for an explanation why
 *   'create' is missing.
 * @param $args
 *   The arguments passed to the method. The keys are holding the following:
 *   0. <entity_type>
 *   1. <uuid>
 *   2. <entity> (only available if $op == 'update')
 */
function _uuid_services_taxonomy_term_bundle_access($op, $args) {
  $entity_ids = entity_get_id_by_uuid('taxonomy_term', array($args[2]));
  $tid = reset($entity_ids);
  if ($tid && $term = taxonomy_term_load($tid)) {
    // If bundle doesn't match, please respond with 404
    if ($term->vocabulary_machine_name != $args[1]) {
      return services_error(t('Term @uuid could not be found', array('@uuid' => $args[2])), 404);
    }
  }

  return TRUE;
}


/**
 * Return an array of optionally paged nids based on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/story?fields=nid,vid&parameters[nid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only nid and vid defined, where
 * nid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @param $bundle
 *   Bundle name to be returned.
 * @return
 *   An array of node objects.
 */
function _uuid_services_taxonomy_term_bundle_index($page, $fields, $parameters, $page_size, $bundle) {
  module_load_include('inc', 'bundle_resource', 'resources/taxonomy_term_bundle_resources');

  return _bundle_resource_taxonomy_term_resource_index($page, $fields, $parameters, $page_size, $bundle);
}

/**
 * Callback for the 'retrieve' method.
 *
 * @see entity_uuid_load()
 */
function _uuid_services_taxonomy_term_bundle_retrieve($entity_type, $bundle_type, $fields, $uuid) {
  module_load_include('inc', 'bundle_resource', 'resources/taxonomy_term_bundle_resources');

  $entity_ids = entity_get_id_by_uuid('taxonomy_term', array($uuid));
  $tid = reset($entity_ids);
  if ($tid) {
    return _bundle_resource_taxonomy_term_resource_retrieve($tid, $fields, $bundle_type);
  }
  else {
    return services_error(t('Node @uuid could not be found', array('@uuid' => $uuid)), 404);
  }
}