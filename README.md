Bundle Resource
===============

Bundle Resource is a simple module that provides separate Services resource entry for each node 
and taxonomy bundles defined in the system. The module is extendible, so it's easy to provide bundle resources 
for other entities using hook_bundle_resources().

The rationale behind this module is to allow developers to define better contracts between Services and client systems.
Contracts offered by general entities like Node or Taxonomy Term resources are hardly defined 
as the fields returned by them differ between specific types. This is opposed by bundles, which have clearly 
defined fields and contracts.

Vanilla module provides only node and taxonomy bundles, but developers can implement hook_bundle_resources()
in order to expose bundles for other entities too.

Installation
============
1) Enable the module
2) Enjoy wealth of bundle resources available for you services.
