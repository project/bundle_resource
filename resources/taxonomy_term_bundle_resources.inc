<?php

/**
 * Returns definition for node bundle resources
 */
function _taxonomy_term_bundle_resources_definition() {
  module_load_include('inc', 'services', 'resources/taxonomy_resource');

  // Retrieve default taxonomy resource definition.
  // It will be used as base for all taxonomy bundle resources.
  $taxonomy_resource_definition = _taxonomy_resource_definition();
  $taxonomy_resource_definition['taxonomy_term']['operations']['index']['callback'] = '_bundle_resource_taxonomy_term_resource_index';
  $taxonomy_resource_definition['taxonomy_term']['operations']['index']['file'] = array('type' => 'inc', 'module' => 'bundle_resource', 'name' => 'resources/taxonomy_term_bundle_resources');

  $taxonomy_resource_definition['taxonomy_term']['operations']['retrieve']['callback'] = '_bundle_resource_taxonomy_term_resource_retrieve';
  $taxonomy_resource_definition['taxonomy_term']['operations']['retrieve']['file'] = array('type' => 'inc', 'module' => 'bundle_resource', 'name' => 'resources/taxonomy_term_bundle_resources');

  // Get all bundles associated with taxonomy term (vocabularies)
  $vocabularies = taxonomy_get_vocabularies();
  $taxonomy_bundles = array();
  foreach($vocabularies as $vocabulary) {
    $fields_arg = array(
      'name' => 'fields',
      'optional' => TRUE,
      'type' => 'string',
      'description' => 'The fields to get.',
      'default value' => '*',
      'source' => array('param' => 'fields'),
    );
    $bundle_arg = array(
      'name' => 'bundle',
      'optional' => TRUE,
      'type' => 'string',
      'description' => 'Bundle name.',
      'default value' => $vocabulary->machine_name,
    );

    $taxonomy_bundles[$vocabulary->machine_name]['operations']['index']['args'][] = $bundle_arg;
    $taxonomy_bundles[$vocabulary->machine_name]['operations']['retrieve']['args'][] = $fields_arg;
    $taxonomy_bundles[$vocabulary->machine_name]['operations']['retrieve']['args'][] = $bundle_arg;
  }

  // Build bundle resources definition for node entity.
  return array('taxonomy_term' => array(
    'entity_resource_definition' => $taxonomy_resource_definition['taxonomy_term'],
    'bundles' => $taxonomy_bundles
  ));
}

/**
 * Return the results of taxonomy_get_term() for a specified term id.
 *
 * @param $tid
 *   Unique identifier for the taxonomy term to retrieve.
 * @param $fields
 *  Fields to be returned.
 * @param $bundle
 *   Bundle name to be returned.
 * @return
 *   A term object.
 *
 * @see taxonomy_get_term()
 */
function _bundle_resource_taxonomy_term_resource_retrieve($tid, $fields, $bundle) {
  if (!empty($fields) && $fields != '*') {
    $fields = explode(',', $fields);
  }
  else {
    $fields = array();
  }

  $term = taxonomy_term_load($tid);

  $filtered_term = new stdClass();

  // Apply filter to fields available on the entity.
  // This also gives opportunity for other modules to modify the field structure itself
  if (!empty($fields)) {
    foreach ($fields as $field) {
      if (isset($term->$field)) {
        $filtered_term->$field = $term->$field;
      }
    }
  }
  else {
    $filtered_term = $term;
  }

  // Enable other modules to alter the node before it's sent in the response
  drupal_alter('bundle_resource_term', $filtered_term, $bundle);

  return $filtered_term;
}

/**
 * Return an array of optionally paged tids baed on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/tags?fields=tid,name&parameters[tid]=7&parameters[vid]=1
 *
 * This would return an array of objects with only tid and name defined, where
 * tid = 7 and vid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @param $vid
 *   VID to limit the terms to
 * @return
 *   An array of term objects.
 *
 * @see _node_resource_index() for more notes
 **/
function _bundle_resource_taxonomy_term_resource_index($page, $fields, $parameters, $page_size, $bundle) {
  // Override vid parameter with vid passed to this function
  $vocabulary = taxonomy_vocabulary_machine_name_load($bundle);
  $parameters['vid'] = $vocabulary->vid;

  global $user;

  if (!empty($fields) && $fields != '*') {
    $fields = explode(',', $fields);
  }
  else {
    $fields = array();
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'taxonomy_term')
    ->propertyCondition('vid', $vocabulary->vid)
    ->range($page * $page_size, $page_size);

  // ?parameters[field_example]=value;>|<|=|<=|>=|>|<>|STARTS_WITH|CONTAINS|IN|NOT IN|BETWEEN;123|100,123
  // Entity Fields: ?parameters[field_date]=value;BETWEEN;1438671600,1439794800
  // Entity Properties (Starts with underscore): ?parameters[_uid]=uid;=;1
  // Use with tokens: ?parameters[_uid]=uid;=;[user:uid]
  if (isset($parameters) && is_array($parameters)) {
    foreach ($parameters as $field => $parameter) {
      $parameter_operation = explode(";", $parameter);
      if (count($parameter_operation) == 3) {
        // If there are any tokens used for values, please replace them with actual values
        $parameter_operation[2] = token_replace($parameter_operation[2], array('user' => $user));

        // Check is value is an array. this is needed to 'between' and IN, NOT IN operator
        if (in_array($parameter_operation[1], array(
          'BETWEEN',
          'IN',
          'NOT IN'
        ))) {
          $parameter_operation[2] = explode(",", $parameter_operation[2]);
        }

        // Properties
        if (substr($field,0,1) == '_') {
          $query->propertyCondition(substr($field, 1), $parameter_operation[2], $parameter_operation[1], 1);
        }

        // Fields
        else {
          $query->fieldCondition($field, $parameter_operation[0], $parameter_operation[2], $parameter_operation[1], 1);
        }
      }
    }
  }

  $entities = $query->execute();
  $terms = taxonomy_term_load_multiple(array_keys($entities['taxonomy_term']));
  $items = array();
  foreach ($terms as $term) {
    $filtered_term = new stdClass();

    // Apply fields limit to this node
    if (!empty($fields)) {
      foreach ($fields as $field) {
        if (isset($term->$field)) {
          $filtered_term->$field = $term->$field;
        }
      }
    }
    else {
      $filtered_term = $term;
    }

    // Enable other modules to alter the term before it's sent in the response
    drupal_alter('bundle_resource_term', $filtered_term, $bundle);

    $items[] = $filtered_term;
  }

  return $items;
}