<?php

/**
 * Returns definition for node bundle resources
 */
function _node_bundle_resources_definition() {
  module_load_include('inc', 'services', 'resources/node_resource');

  // Retrieve default node resource definition.
  // It will be used as base for all node bundle resources.
  $node_resource_definition = _node_resource_definition();
  $node_resource_definition['node']['operations']['index']['callback'] = '_bundle_resource_node_resource_index';
  $node_resource_definition['node']['operations']['index']['file'] = array('type' => 'inc', 'module' => 'bundle_resource', 'name' => 'resources/node_bundle_resources');

  $node_resource_definition['node']['operations']['retrieve']['callback'] = '_bundle_resource_node_resource_retrieve';
  $node_resource_definition['node']['operations']['retrieve']['access callback'] = '_bundle_resource_node_resource_access';
  $node_resource_definition['node']['operations']['retrieve']['file'] = array('type' => 'inc', 'module' => 'bundle_resource', 'name' => 'resources/node_bundle_resources');

  // Get all bundles of node and prepare bundles array
  // This array overrides any default entity resource settings defined above
  $types = node_type_get_types();
  $node_bundles = array();
  foreach($types as $type_name => $type_definition) {
    $fields_arg = array(
      'name' => 'fields',
      'optional' => TRUE,
      'type' => 'string',
      'description' => 'The fields to get.',
      'default value' => '*',
      'source' => array('param' => 'fields'),
    );
    $bundle_arg = array(
      'name' => 'bundle',
      'optional' => TRUE,
      'type' => 'string',
      'description' => 'Bundle name.',
      'default value' => $type_name,
    );

    $node_bundles[$type_name]['operations']['index']['args'][] = $bundle_arg;
    $node_bundles[$type_name]['operations']['retrieve']['args'][] = $fields_arg;
    $node_bundles[$type_name]['operations']['retrieve']['args'][] = $bundle_arg;
  }

  return array('node' => array(
    'entity_resource_definition' => $node_resource_definition['node'],
    'bundles' => $node_bundles
  ));
}

/**
 * Determine whether the current user can access a node resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see node_access()
 */
function _bundle_resource_node_resource_access($op = 'view', $args = array()) {
  // Adds backwards compatability with regression fixed in #1083242
  if (isset($args[0])) {
    $args[0] = _services_access_value($args[0], 'node');
  }

  // Make sure we have an object or this all fails, some servers can
  // mess up the types.
  if (is_array($args[0])) {
    $args[0] = (object) $args[0];
  }
  elseif (!is_array($args[0]) && !is_object($args[0])) {  //This is to determine if it is just a string happens on node/%NID
    $args[0] = (object)array('nid' => $args[0]);
  }

  // Make sure we have an object or this all fails, some servers can
  // mess up the types.
  if (is_array($args[1])) {
    $args[1] = (object) $args[1];
  }
  elseif (!is_array($args[0]) && !is_object($args[1])) {  //This is to determine if it is just a string happens on node/%NID
    $args[1] = (object)array('bundle' => $args[1]);
  }

  if ($op != 'create' && !empty($args)) {
    $node = node_load($args[0]->nid);
    if ($node && $node->type = $args[1]->bundle) {
      return _node_resource_access($op, $args);
    }
  }

  return services_error(t('Node @nid could not be found', array('@nid' => $args[0]->nid)), 404);
}

/**
 * Returns the results of a node_load() for the specified node.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the node we want to return.
 * @param $fields
 *  Fields to be returned.
 * @param $bundle
 *   Bundle name to be returned.
 * @return
 *   Node object or FALSE if not found.
 *
 * @see node_load()
 */
function _bundle_resource_node_resource_retrieve($nid, $fields, $bundle) {
  if (!empty($fields) && $fields != '*') {
    $fields = explode(',', $fields);
  }
  else {
    $fields = array();
  }

  $node = node_load($nid);

  if ($node) {
    $uri = entity_uri('node', $node);
    $node->path = url($uri['path'], array('absolute' => TRUE));
    // Unset uri as it has complete entity and this
    // cause never ending recursion in rendering.
    unset($node->uri);
  }

  $filtered_node = new stdClass();

  // Apply filter to fields available on the entity.
  // This also gives opportunity for other modules to modify the field structure itself
  if (!empty($fields)) {
    foreach ($fields as $field) {
      if (isset($node->$field)) {
        $filtered_node->$field = $node->$field;
      }
    }
  }
  else {
    $filtered_node = $node;
  }

  // Enable other modules to alter the node before it's sent in the response
  drupal_alter('bundle_resource_node', $filtered_node, $bundle);

  return $filtered_node;
}

/**
 * Return an array of optionally paged nids based on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/story?fields=nid,vid&parameters[nid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only nid and vid defined, where
 * nid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @param $bundle
 *   Bundle name to be returned.
 * @return
 *   An array of node objects.
 */
function _bundle_resource_node_resource_index($page, $fields, $parameters, $page_size, $bundle) {
  global $user;

  if (!empty($fields) && $fields != '*') {
    $fields = explode(',', $fields);
  }
  else {
    $fields = array();
  }

  // We introduced parameter sets to support OR queries.
  // WARNING: This is achieved by making multiple queries to simulate OR, so please use sparingly.
  $parameter_sets = array($parameters);
  drupal_alter('bundle_resource_node_parameter_set', $parameter_sets, $parameters, $bundle);

  $node_set = array();
  foreach($parameter_sets as $params) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $bundle)
      ->range($page * $page_size, $page_size);

    // Check if this bundle should check for node_access
    // Default is 'yes'. Developers can turn it off by setting relevant variable to FALSE;
    if (variable_get('bundle_resource_node_access_' . $bundle, TRUE)) {
      $query->addTag('node_access')->addMetaData('account', $user);
    } else {
      $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
    }

    // ?parameters[field_example]=value;>|<|=|<=|>=|>|<>|STARTS_WITH|CONTAINS|IN|NOT IN|BETWEEN;123|100,123
    // Entity Fields: ?parameters[field_date]=value;BETWEEN;1438671600,1439794800
    // Entity Properties (Starts with underscore): ?parameters[_uid]=uid;=;1
    // Use with tokens: ?parameters[_uid]=uid;=;[user:uid]
    if (isset($params) && is_array($params)) {
      foreach ($params as $field => $parameter) {
        $parameter_operation = explode(";", $parameter);
        if (count($parameter_operation) == 3) {
          // If there are any tokens used for values, please replace them with actual values
          $parameter_operation[2] = token_replace($parameter_operation[2], array('user' => $user));

          // Handle special parameters
          // UUID discovery for entity references.

          // Check is value is an array. this is needed to 'between' and IN, NOT IN operator
          if (in_array($parameter_operation[1], array(
            'BETWEEN',
            'IN',
            'NOT IN'
          ))) {
            $parameter_operation[2] = explode(",", $parameter_operation[2]);
          }

          // Properties
          if (substr($field,0,1) == '_') {
            $query->propertyCondition(substr($field, 1), $parameter_operation[2], $parameter_operation[1]);
          }

          // Fields
          else {
            $query->fieldCondition($field, $parameter_operation[0], $parameter_operation[2], $parameter_operation[1]);
          }
        }
      }
    }

    if (!user_access('administer nodes')) {
      $query->propertyCondition('status', NODE_PUBLISHED);
    }

    $entities = $query->execute();

    if (!empty($entities['node'])) {
      $node_set = array_merge($node_set, array_keys($entities['node']));
    }
  }

  $nodes = node_load_multiple(array_unique($node_set));
  $items = array();
  foreach ($nodes as $node) {
    $filtered_node = new stdClass();

    // Apply filter to fields available on the entity.
    // This also gives opportunity for other modules to modify the field structure itself
    if (!empty($fields)) {
      foreach ($fields as $field) {
        if (isset($node->$field)) {
          $filtered_node->$field = $node->$field;
        }
      }
    }
    else {
      $filtered_node = $node;
    }

    // Enable other modules to alter the node before it's sent in the response
    drupal_alter('bundle_resource_node', $filtered_node, $bundle);

    $items[] = $filtered_node;
  }

  return $items;
}